/**
 * Extend the base Actor entity by defining a custom roll data structure.
 * @extends {Actor}
 */

import {  getModGsTs } from "../lfgutils.js";

export class LFGActor extends Actor {

  /**
   * Augment the basic actor data with additional dynamic data.
   */
  prepareData() {
    if(!this.data.img) { this.data.img = './systems/lfg/images/lfg-icon.png' }

    super.prepareData();

    const actorData = this.data;
    const data = actorData.data;
    const flags = actorData.flags;

    // Make separate methods for each Actor type (character, npc, etc.) to keep
    // things organized.
    if (actorData.type === 'character') this._prepareCharacterData(actorData);
    
    if (actorData.type === 'npc') this._prepareNpcData(actorData);
  }

  /**
   * Prepare Character type specific data
   */
  _prepareCharacterData(actorData) {
    const data = actorData.data;
    // Make modifications to data here.
    // // Loop through ability scores, and add their modifiers to our sheet output.
    for (let [key, ability] of Object.entries(data.abilities)) {
      data.abilities[key] = { ...ability, ...getModGsTs(ability.value) };
    }
    for (let [key, skill] of Object.entries(data.skills)) {
      data.skills[key] = { ...skill, ...getModGsTs(data.abilities[skill.attr].value) };
    }
  }

  _prepareNpcData(actorData) {
    const data = actorData.data;
    // Make modifications to data here.
    // Loop through ability scores, and add their modifiers to our sheet output.
    for (let [key, ability] of Object.entries(data.abilities)) {
      data.abilities[key] = getModGsTs(ability.value)
    }
  }

}
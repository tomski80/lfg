/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
import { lfgRoll, ddmRoll, ddmMessage, successLevelMessage } from "../lfgutils.js";

export class LFGActorSheet extends ActorSheet {
  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["lfg", "sheet", "actor"],
      width: 900,
      height: 600,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description" }]
    });
  }

  /** @override */
get template() {
  if (this.actor.data.type == 'character') {
    return `systems/lfg/templates/actor/actor-sheet.html`;
  } else if (this.actor.data.type == 'npc') {
    return `systems/lfg/templates/actor/npc-sheet.html`;
  }
}

  /* -------------------------------------------- */
  
  /** @override */
  getData() {
    const data = super.getData();
    data.dtypes = ["String", "Number", "Boolean"];

    // Prepare items.
    if (this.actor.data.type == 'character') {
      this._prepareCharacterItems(data);
    }

    return data;
  }

  /**
   * Organize and classify Items for Character sheets.
   *
   * @param {Object} actorData The actor to prepare.
   *
   * @return {undefined}
   */
  _prepareCharacterItems(sheetData) {
    const actorData = sheetData.actor;

    // Initialize containers.
    const gear = [];
    const weapons = [];
    const features = [];
    const skills = [];
    const spells = {
      1: [],
      2: [],
      3: [],
      4: [],
      5: [],
      6: []
    };
    const injSetbacks = [];
    const madness = [];
    const darkMagic = [];

    // Iterate through items, allocating to containers
    // let totalWeight = 0;
    for (let i of sheetData.items) {
      let item = i.data;
      i.img = i.img || DEFAULT_TOKEN;
      // Append to gear.
      if (i.type === 'item') {
        gear.push(i);
      }
      // Append to weapons.
      if (i.type === 'weapon') {
        weapons.push(i);
      }
      // Append to features.
      else if (i.type === 'feature') {
        features.push(i);
      }
      // Append to spells.
      else if (i.type === 'spell') {
        if (i.data.spellLevel != undefined) {
          spells[i.data.spellLevel].push(i);
        }
      }
       // Append to injuries and setbacks.
      else if (i.type === 'injSetback') {
        injSetbacks.push(i);
      }
      else if (i.type === 'madness') {
        madness.push(i);
      }
      else if (i.type === 'darkMagic') {
        darkMagic.push(i);
      }
    }

    // Assign and return
    actorData.gear = gear;
    actorData.weapons = weapons;
    actorData.features = features;
    actorData.skills = skills;
    actorData.spells = spells;
    actorData.injSetbacks = injSetbacks;
    actorData.madness = madness;
    actorData.darkMagic = darkMagic;
  }

  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    html.find('.skill-proficient').on("click contextmenu", this._onToggleSkillProficiency.bind(this));

    // Add Inventory Item
    html.find('.item-create').click(this._onItemCreate.bind(this));

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.getOwnedItem(li.data("itemId"));
      item.sheet.render(true);
    });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      this.actor.deleteOwnedItem(li.data("itemId"));
      li.slideUp(200, () => this.render(false));
    });

    // Rollable abilities.
    html.find('.rollable').click(this._onRoll.bind(this));
    html.find('.rollable-luck').click(this._onRollLuck.bind(this));
    html.find('.rollable-ddm').click(this._onRollDDM.bind(this));

    // Drag events for macros.
    if (this.actor.owner) {
      let handler = ev => this._onDragItemStart(ev);
      html.find('li.item').each((i, li) => {
        if (li.classList.contains("inventory-header")) return;
        li.setAttribute("draggable", true);
        li.addEventListener("dragstart", handler, false);
      });
    }

    // Rollable NPC HD and NoAppear
    html.find('.npc-rollable').click(this._npcRoll.bind(this));
  }

  /**
   * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
   * @param {Event} event   The originating click event
   * @private
   */
  _onItemCreate(event) {
    event.preventDefault();
    const header = event.currentTarget;
    // Get the type of item to create.
    const type = header.dataset.type;
    // Grab any data associated with this control.
    const data = duplicate(header.dataset);
    // Initialize a default name.
    const name = `New ${type.capitalize()}`;
    // Prepare the item object.
    const itemData = {
      name: name,
      type: type,
      data: data
    };
    // Remove the type from the dataset since it's in the itemData.type prop.
    delete itemData.data["type"];

    // Finally, create the item!
    return this.actor.createOwnedItem(itemData);
  }

  getLuckModifer(){
    const modifier = $('#luck-modifier')[0].value;
    if (!modifier){
      return 0;
    }
    return this.actor.data.data.abilities[modifier].mod;
  }
  roll(data) {
    const result = lfgRoll(data);
    result.roll
      .toMessage({ 
        speaker: ChatMessage.getSpeaker({ actor: this.actor }), 
        flavor: successLevelMessage(result) });
    return result;
  }
  magicRoll(data) {
    const result = ddmRoll(data);
    result.roll
      .toMessage({
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: ddmMessage(result)
      });
    return result;
  }
  async incrementDDM(){
    if (this.actor.data.data.effects.ddm.value <= 0){
      return false;
    }
    try{
      await this.actor.update({[`data.effects.ddm.value`]: this.actor.data.data.effects.ddm.value + 1}); // Updates one EmbeddedEntity
    } catch(e){
      console.log(e)
    }
  }
  async resetDDM(){
    if (this.actor.data.data.effects.ddm.value <= 0){
      return false;
    }
    try{
      await this.actor.update({[`data.effects.ddm.value`]: 1}); // Updates one EmbeddedEntity
    } catch(e) {
      console.log(e)
    }
  }
  async decrementLuck(){
    if (this.actor.data.data.resources.luck.value <= 0){
      return false;
    }
    try{
      await this.actor.update({[`data.resources.luck.value`]: this.actor.data.data.resources.luck.value - 1}); // Updates one EmbeddedEntity
    } catch(e){
      console.log(e)
    }
  }
  /**
   * Handle clickable rolls.
   * @param {Event} event   The originating click event
   * @private
   */

  _onRollLuck(event) {
    event.preventDefault();
    const result = this.roll({...event.currentTarget.dataset, mod: this.getLuckModifer()});
    if (result.roll.total <= result.target ){
      this.decrementLuck();
    }
  }
  _onRoll(event) {
    event.preventDefault();
    this.roll(event.currentTarget.dataset);
  }

  _onRollDDM(event) {
    event.preventDefault();
    const result = this.magicRoll(event.currentTarget.dataset);
    if (result.roll.total > result.target ){
      this.incrementDDM();
    } else {
      this.resetDDM();
    }
  }

  async _onToggleSkillProficiency(event) {
    event.preventDefault();
    const name = event.currentTarget.dataset.skill
    try{
      await this.actor.update({[`data.skills.${name}.skilled`]: !this.actor.data.data.skills[name].skilled}); // Updates one EmbeddedEntity
    } catch(e){
      console.log(e)
    }
  }

  _npcRoll(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;

    if (dataset.roll) {
      let roll = new Roll(dataset.roll, this.actor.data.data);
      let label = dataset.label ? `${dataset.label}` : '';
      roll.roll().toMessage({
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: label
      });
    }
  }
}

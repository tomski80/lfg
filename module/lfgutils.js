function lfgRoll({ value = 10, skilled, label = '', mod = 0, dice = '1d20'}) {
  const skillMod = skilled === "true" ? 1 : 0;
  const target = parseInt(value, 10) + skillMod + mod;
  let { gs, tf } = getModGsTs(target);
  let rollFormula = dice;
  const roll = new Roll( rollFormula).roll();
  return { roll, target, gs, tf, label, skilled: skillMod }
}

function ddmRoll({ value = 1, label = '', dice = '1d20'}) {
  const target = parseInt(value, 10);
  let rollFormula = dice;
  const roll = new Roll( rollFormula ).roll();
  return { roll, target, label }
}

function successLevelMessage(result){
  let skillMessage = result.skilled ? "(skilled)" : "";
  let message = `${result.label.toUpperCase()} ${skillMessage} Target: ${result.target}`
  if (result.gs && result.tf) {
    message = `${message}  GS: ${result.gs} TF: ${result.tf}`
  }
  if (result.roll.total >= result.tf )
    return `Terrible Failure (${message})`;
  if (result.roll.total > result.target ) 
    return `Failure (${message})`;
  if (result.roll.total <= result.gs ) 
    return `Greater Success (${message})`;
  return `Success (${message})`;
}

function ddmMessage(result){
  let message = `${result.label} Target: ${result.target}`
  if (result.roll.total <= result.target)
    return `Failure (${message}). Roll on DDM Table`;
  return `Success (${message})`;
}

function getModGsTs(value) {
  const gs = Math.floor(value / 2)
  const tf = Math.floor(value * 1.5)
  let mod = 0;
  if (value >= 19 ) mod = 4;
  if ([17, 18].includes(value)) mod = 3;
  if ([15, 16].includes(value)) mod = 2;
  if ([13, 14].includes(value)) mod = 1;
  if ([7, 8].includes(value)) mod = -1;
  if ([5, 6].includes(value)) mod = -2;
  if ([3, 4].includes(value)) mod = -3;
  if ( value <= 2 ) mod = -4
  
  return { value, mod, gs, tf }
}

export { lfgRoll, ddmRoll, ddmMessage, getModGsTs, successLevelMessage }
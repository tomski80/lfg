## lowfantasygaming

This version of the Low Fantasy Gaming system for Foundry VTT has been coded with permission for distribution
from Pickpocket Press. It includes a character sheet and system support for Low Fantasy Gaming.

## To Install

Simply download the .zip file or clone the repository and extract it into Foundry's Data/systems/lfg folder. *After extracting, make sure the name of the containing folder is set to 'lfg' and not 'lfg-master'*

The Free PDF version of Low Fantasy Gaming is found at https://lowfantasygaming.com/freepdf/.

Code is modeled after the tutorial created by Matt Smith found at https://gitlab.com/asacolips-projects/foundry-mods/foundryvtt-system-tutorial using many of the files from the boilerplate system.

Focus of Version 1.0.0 is a basic character sheet and base rule system for easy use in Foundry. It includes simple rollable buttons for Luck, Attributes, and Dark and Dangerous Magic, as well as a place to store all character info, including items, injuries, basic stats, madness traits, etc.

Plans for future versions include: 1) more system appropriate styling, 2) rollable macros for weapon attacks and spells, 3) compendiums of monsters, items, weapons, and rollable tables from the free version of the Low Fantasy Gaming PDF, 4) other improvements as noted by the amazing Low Fantasy Gaming community.

Any questions, comments, concerns, etc. may be emailed to development.devin@gmail.com or communicated through gitlab.

## Version 1.1.4

Update includes:

1) Improved DDM Rolls. DDM Target is now displayed properly instead of a constant "10" and success/failure is now noted. Additionally, DDM Target automatically increases by +1 on a success and resets to 1 on a failure (while also prompting the owner to roll on the DDM Table)

2) A compendium of all Roll Tables from the Free PDF version of Low Fantasy Gaming, thanks to Alain Choiniere! Most everything is auto-rolled for the one rolling, from damage to number appearing. Luck Saves and Percentages do not auto-roll.

## Version 1.1.3

Update includes:

1) Adding Spell Slots for tracking purposes

## Version 1.1.2

Update includes:

1) Tested for compatibility with FoundryVTT version 0.7.9

2) Improved Skills list, with appropriate rolls, coded by Guin Jane

3) Improved Luck Rolls with selectable attribute and automatic Luck reduction on successful checks coded by Guin Jane

## Version 1.1.1

Update includes:

1) Tested for compatibility with FoundryVTT version 0.7.7 and updated in system.json

2) Improved attribute rolls, including levels of success, coded by Guin Jane

## Version 1.1.0

Update includes:

1) Tested for compatibility with FoundryVTT version 0.7.7 and updated in system.json

2) Added NPC actor type for simpler sheets. For use with both NPCs and Monsters

3) Added Monster Compendium for the Free PDF version of Low Fantasy Gaming, with included place-holder images from game-icons.net

4) Added Weapon Attacks (Macro) compendium, with included place-holder images from game-icons.net

## Version 1.0.2

Small update including:

1) Tested for compatibility with FoundryVTT version 0.7.6 and updated in system.json

2) Added svg icons for weapon macros (macros coming in next update)

## Version 1.0.1

This small update includes the following:

1) Tested for compatibility with FoundryVTT version 0.7.5 and updated in system.json

2) Updated manifest to fix issue with checking for updates through FoundryVTT's Game Systems tab

3) Changed default token image to LFG icon provided by Pickpocket Press and edited with permission

4) Changed default font to Aquifer to match LFG branding
